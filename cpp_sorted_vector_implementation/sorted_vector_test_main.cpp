#define BOOST_TEST_MODULE sorted_vector_test

#include "sorted_vector.hpp"

#include <iostream>
#include <vector>
#include <string>
#include <boost\test\auto_unit_test.hpp>
using namespace std;

BOOST_AUTO_TEST_CASE(ctor) {
	{
		sorted_vector<int> v;
		BOOST_CHECK(v.size() == 0);
		BOOST_CHECK(v.empty() == true);
		BOOST_CHECK(v.capacity() == 0);
	}
	{
		sorted_vector<string> v(5);
		BOOST_CHECK(v.size() == 5);
		BOOST_CHECK(v.empty() == false);
		BOOST_CHECK(v.capacity() == 5);
	}
	{
		sorted_vector<int> v(10, 34);
		BOOST_CHECK(v.size() == 10);
		BOOST_CHECK(v.empty() == false);
		BOOST_CHECK(v.capacity() == 10);

		for (unsigned i = 0; i < v.size(); ++i)
			BOOST_CHECK(v[i] == 34);
	}
	{
		sorted_vector<int> v(10, 34);
		sorted_vector<int> copy(v);
		BOOST_CHECK(v.size() == copy.size());
		BOOST_CHECK(v.empty() == copy.empty());
		BOOST_CHECK(v.capacity() == copy.capacity());

		for (unsigned i = 0; i < v.size(); ++i)
			BOOST_CHECK(v[i] == copy[i]);

		copy[1] = 4;
		BOOST_CHECK(v[1] == 34);
	}
	{
		sorted_vector<int> v{ 1,2,3,4,5,6,7 };
		BOOST_CHECK(v.size() == 7);
		BOOST_CHECK(v.empty() == false);
		BOOST_CHECK(v.capacity() == 7);

		for (unsigned i = 0; i < v.size(); ++i)
			BOOST_CHECK(v[i] == i + 1);
	}
	{
		sorted_vector<int> v{ 2,3,5,4,7,1,6 };
		BOOST_CHECK(v.size() == 7);
		BOOST_CHECK(v.empty() == false);
		BOOST_CHECK(v.capacity() == 7);

		for (unsigned i = 0; i < v.size(); ++i)
			BOOST_CHECK(v[i] == i + 1);
	}
}

BOOST_AUTO_TEST_CASE(insert) {

	sorted_vector<int> v;
	v.insert(32);
	BOOST_CHECK(v.size() == 1);
	BOOST_CHECK(v.empty() == false);
	BOOST_CHECK(v.capacity() == 1);
	v.insert(43);
	BOOST_CHECK(v.size() == 2);
	BOOST_CHECK(v.empty() == false);
	BOOST_CHECK(v.capacity() == 2);
	v.insert(34);
	BOOST_CHECK(v.size() == 3);
	BOOST_CHECK(v.empty() == false);
	BOOST_CHECK(v.capacity() == 4);

	BOOST_CHECK(v[1] == 34);
}

BOOST_AUTO_TEST_CASE(iterators) {
	sorted_vector<int> v;
	sorted_vector<int> stdv;

	for (int i = 0; i < 10; ++i) {
		v.insert(i + 1);
		stdv.insert(i + 1);
	}

	BOOST_CHECK(stdv.size() == v.size());

	auto stdx = stdv.begin();
	for (auto x : v) 
		BOOST_CHECK(x == *(stdx++));	

	auto std_rit = stdv.rbegin();
	for (auto rit = v.rbegin(); rit != v.rend(); ++rit) {
		BOOST_CHECK(*rit == *(std_rit++));
	}

	BOOST_CHECK(v.pop_back() == 10);
	BOOST_CHECK(v.size() == 9);
}

BOOST_AUTO_TEST_CASE(erase) {
	{
		sorted_vector<int> v;
		for (int i = 0; i < 10; ++i) 
			v.insert(i + 1);
		
		v.erase(sorted_vector<int>::iterator(v.begin() + 5));

		BOOST_CHECK(v.size() == 5);
		for (unsigned i = 0; i < v.size(); ++i)
			BOOST_CHECK(v[i] == i + 1);	
	}
	{
		sorted_vector<int> v;
		for (int i = 0; i < 20; ++i) 
			v.insert(i + 1);
		
		v.erase(sorted_vector<int>::iterator(v.begin() + 10), sorted_vector<int>::iterator(v.begin() + 20));

		BOOST_CHECK(v.size() == 10);
		for (unsigned i = 0; i < v.size(); ++i)
			BOOST_CHECK(v[i] == i + 1);
	}
}

BOOST_AUTO_TEST_CASE(other_modifiers) {
	{
		sorted_vector<int> v(10, 10);	
		sorted_vector<int> v2(2, 20);

		v2 = v;
		BOOST_CHECK(v2.size() == v.size());
		BOOST_CHECK(v2.capacity() == v.capacity());
		for (unsigned i = 0; i < v.size(); ++i)
			BOOST_CHECK(v[i] == v2[i]);
	}
	{
		sorted_vector<int> v(15, 2);

		v.reserve(3);
		BOOST_CHECK(v.capacity() == 15);

		v.reserve(20);
		BOOST_CHECK(v.capacity() == 20);

		v.shrink_to_fit();
		BOOST_CHECK(v.capacity() == 15);
	}
}