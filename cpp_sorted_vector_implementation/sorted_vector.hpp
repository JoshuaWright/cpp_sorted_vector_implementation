/*
Author:	Joshua wright joshuawright1197@gmail.com
Date:	2017-05-21
File:	sorted_vector.hpp
*/

#pragma once

#include <iterator>
#include <exception>
#include <cstdlib>

template<typename T,typename Compare = std::less<T>,typename Allocator = std::allocator<T>>
class sorted_vector {
public:

	// TYPES
	using size_type					= std::size_t;
	using value_type				= T;
	using value_compare				= Compare;
	using allocator_type			= Allocator;
	using pointer					= value_type*;
	using const_pointer				= const value_type*;
	using reference					= value_type&;
	using const_reference			= const value_type&;
	using iterator					= pointer;
	using const_iterator			= const pointer;
using reverse_iterator = std::reverse_iterator<iterator>;
using const_reverse_iterator = std::reverse_iterator<const_iterator>;

private:

	// MEMBERS
	pointer			begPtr;
	pointer			endPtr;
	pointer			capPtr;
	allocator_type	allocator;

public:

// CONSTRUCTORS
sorted_vector() : begPtr(nullptr), endPtr(nullptr), capPtr(nullptr) {}
explicit sorted_vector(size_type const&);
explicit sorted_vector(size_type const&, value_type const&);
sorted_vector(std::initializer_list<T> const&);
sorted_vector(sorted_vector const&);

// DESTRUCTOR
~sorted_vector() { clear(); }

// ELEMENT ACCESS
reference		operator [] (size_type const& idx) { return begPtr[idx]; }
const_reference operator [] (size_type const& idx) const { return begPtr[idx]; }
reference		at(size_type const);
const_reference at(size_type const) const;
reference		back() { return *(endPtr - 1); }
const_reference	back() const { return *(endPtr - 1); }
reference		front() { return *begPtr; }
const_reference	front() const { return *begPtr; }
pointer			data() { return begPtr; }
const_pointer	data() const { return begPtr; }

// CAPACITY
size_type		size() const { return endPtr - begPtr; }
bool			empty() const { return size() == 0; }
size_type		capacity() const { return capPtr - begPtr; }
allocator_type	get_allocator() const { return allocator; }

// ITERATORS
iterator				begin() { return iterator(begPtr); }
iterator				end() { return iterator(endPtr); }
const_iterator			begin()	const { return const_iterator(begPtr); }
const_iterator			end() const { return const_iterator(endPtr); }
const_iterator			cbegin() const { return const_iterator(begPtr); }
const_iterator			cend() const { return const_iterator(endPtr); }
reverse_iterator		rbegin() { return reverse_iterator(endPtr); }
reverse_iterator		rend() { return reverse_iterator(begPtr); }
const_reverse_iterator	rbegin() const { return const_reverse_iterator(endPtr); }
const_reverse_iterator	rend() const { return const_reverse_iterator(begPtr); }
const_reverse_iterator	crbegin() const { return const_reverse_iterator(endPtr); }
const_reverse_iterator	crend() const { return const_reverse_iterator(begPtr); }

// MODIFIERS
void			clear();
void			insert(value_type const&);
value_type		pop_back();
void			reserve(size_type const&);
void			shrink_to_fit();
iterator		erase(iterator const&);
iterator		erase(iterator const&, iterator const&);
sorted_vector&	operator=(sorted_vector const&);

private:
	// HELPERS  
	void sort(size_type);
	void reAllocate(size_type const&);
	void shift(size_type const&, size_type const& = 0);

}; // end class Vector

template<typename T, typename Compare = std::less<T>, typename Allocator = std::allocator<T>>
sorted_vector<T, Compare, Allocator>::sorted_vector(size_type const& size) {
	begPtr = allocator.allocate(size);
	capPtr = endPtr = begPtr + size;
}

template<typename T, typename Compare = std::less<T>, typename Allocator = std::allocator<T>>
sorted_vector<T, Compare, Allocator>::sorted_vector(size_type const& size, value_type const& value) {
	begPtr = allocator.allocate(size);
	capPtr = endPtr = begPtr + size;
	for (size_type i = 0; i < size; ++i) {
		allocator.construct(begPtr + i, value);
	}
}

template<typename T, typename Compare = std::less<T>, typename Allocator = std::allocator<T>>
sorted_vector<T, Compare, Allocator>::sorted_vector(std::initializer_list<T> const& init) {
	capPtr = endPtr = begPtr = allocator.allocate(init.size());
	capPtr += init.size();
	for (auto& e : init)
		insert(e);
}

template<typename T, typename Compare, typename Allocator>
sorted_vector<T, Compare, Allocator>::sorted_vector(sorted_vector const & toBeCopyed) {
	begPtr = allocator.allocate(toBeCopyed.capacity());
	capPtr = begPtr + toBeCopyed.capacity();

	for (unsigned i = 0; i < toBeCopyed.size(); ++i)
		allocator.construct(begPtr + i, toBeCopyed[i]);

	endPtr = begPtr + toBeCopyed.size();
}

template<typename T, typename Compare, typename Allocator>
sorted_vector<T, Compare, Allocator>& sorted_vector<T, Compare, Allocator>::operator=(sorted_vector const& toBeCopyed)
{
	clear();

	if ((size() == toBeCopyed.size()) && (capacity() == toBeCopyed.capacity())) {
		for (unsigned i = 0; i < toBeCopyed.size(); ++i)
			begPtr[i] == toBeCopyed[i];
		return *this;
	}

	begPtr = allocator.allocate(toBeCopyed.capacity());
	capPtr = begPtr + toBeCopyed.capacity();

	for (unsigned i = 0; i < toBeCopyed.size(); ++i)
		allocator.construct(begPtr + i, toBeCopyed[i]);

	endPtr = begPtr + toBeCopyed.size();

	return *this;
}

template<typename T, typename Compare = std::less<T>, typename Allocator = std::allocator<T>>
void sorted_vector<T, Compare, Allocator>::insert(value_type const& value) {
	if (endPtr == capPtr) {
		int newCap = 0;

		if (size() != 0) 
			newCap = (capacity() * 2);
		else 
			newCap = 1;
	
		reAllocate(newCap);
	}
	begPtr[size()] = value;
	++endPtr;
	sort(size() - 1);
}

template<typename T, typename Compare = std::less<T>, typename Allocator = std::allocator<T>>
typename sorted_vector<T, Compare, Allocator>::value_type sorted_vector<T, Compare, Allocator>::pop_back() {
	value_type result = begPtr[size() - 1];
	allocator.destroy(begPtr + (size() - 1));
	--endPtr;
	return result;
}

template<typename T, typename Compare, typename Allocator>
void sorted_vector<T, Compare, Allocator>::clear() {
	for (pointer p = begPtr; p != endPtr; ++p)
		allocator.destroy(p);
	allocator.deallocate(begPtr, size());
}

template<typename T, typename Compare = std::less<T>, typename Allocator = std::allocator<T>>
typename sorted_vector<T, Compare, Allocator>::reference sorted_vector<T, Compare, Allocator>::at(size_type const idx) {
	if (idx >= size())
		throw std::out_of_range("index out of range");
	return begPtr[idx];
}

template<typename T, typename Compare = std::less<T>, typename Allocator = std::allocator<T>>
typename sorted_vector<T, Compare, Allocator>::const_reference sorted_vector<T, Compare, Allocator>::at(size_type const idx) const {
	if (idx >= size())
		throw std::out_of_range("index out of range");
	return begPtr[idx];
}

template<typename T, typename Compare, typename Allocator = std::allocator<T>>
void sorted_vector<T, Compare, Allocator>::sort(size_type index) {
	if (index > 0) {
		if (Compare()(begPtr[index], begPtr[index - 1])) {
			std::swap(begPtr[index], begPtr[index - 1]);
			return sort(--index);
		}
	}
}

template<typename T, typename Compare, typename Allocator = std::allocator<T>>
void sorted_vector<T, Compare, Allocator>::reAllocate(size_type const& newCap) {
	pointer newBeg = allocator.allocate(newCap);

	for (size_type i = 0; i < size(); ++i) 
		allocator.construct(newBeg + i, begPtr[i]);

	clear();

	capPtr = newBeg + newCap;
	endPtr = newBeg + size();

	begPtr = newBeg;
}

template<typename T, typename Compare, typename Allocator = std::allocator<T>>
void sorted_vector<T, Compare, Allocator>::reserve(size_type const& newCap) {
	if(newCap > capacity())
		reAllocate(newCap);
}

template<typename T, typename Compare, typename Allocator = std::allocator<T>>
void sorted_vector<T, Compare, Allocator>::shrink_to_fit(){
	reAllocate(size());
}

template<typename T, typename Compare, typename Allocator = std::allocator<T>>
typename sorted_vector<T, Compare, Allocator>::iterator sorted_vector<T, Compare, Allocator>::erase(iterator const& erase_point) {
	shift(erase_point - begin());
	return erase_point;
}

template<typename T, typename Compare, typename Allocator = std::allocator<T>>
typename sorted_vector<T, Compare, Allocator>::iterator sorted_vector<T, Compare, Allocator>::erase(iterator const& start_it, iterator const& end_it) {
	shift(start_it - begin(),end_it - begin() );
	return start_it;
}

template<typename T, typename Compare, typename Allocator = std::allocator<T>>
void sorted_vector<T, Compare, Allocator>::shift(size_type const& start_idx,size_type const& end_idx) {
	size_type toBeMovedSize = size() - end_idx;
	pointer toBeMoved = allocator.allocate(toBeMovedSize);

	int ii = 0;
	for (size_type i = end_idx; i < size(); ++i) 
		toBeMoved[ii++] = begPtr[i];
	
	ii = start_idx;
	for (size_type i = 0; i < size() - end_idx; ++i) 
		begPtr[ii++] = toBeMoved[i];
	
	for (pointer p = toBeMoved; p != (toBeMoved + toBeMovedSize); ++p)
		allocator.destroy(p);

	allocator.deallocate(toBeMoved,toBeMovedSize);

	int num_destroys = std::labs(start_idx - end_idx);
	for (int i = 0; i < num_destroys; ++i) {
		allocator.destroy(begPtr + (size() - 1));
		--endPtr;
	}		
}